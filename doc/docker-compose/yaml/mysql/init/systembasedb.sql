/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.20 : Database - systembasedb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`systembasedb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `systembasedb`;

/*Table structure for table `t_black_list` */

DROP TABLE IF EXISTS `t_black_list`;

CREATE TABLE `t_black_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据值',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '引入数据字典 1.用户名 2.ip',
  `create_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `update_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_black_list` */

/*Table structure for table `t_dictionary` */

DROP TABLE IF EXISTS `t_dictionary`;

CREATE TABLE `t_dictionary` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据名称',
  `value` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据值',
  `meaning` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '含义',
  `create_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_user` varchar(40) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL COMMENT '修改人',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据字典';

/*Data for the table `t_dictionary` */

insert  into `t_dictionary`(`id`,`name`,`value`,`meaning`,`create_user`,`create_time`,`update_user`,`update_time`,`remark`) values (1,'RESOURCETYPE_ID','0','菜单','admin','2021-03-02 09:12:15','admin','2021-03-02 09:12:15','资源类型'),(2,'RESOURCETYPE_ID','1','功能','admin','2021-03-02 09:12:15','admin','2021-03-02 09:12:15','资源类型'),(3,'BLACKLIST_TYPE','0','用户名','admin','2021-04-12 15:05:05','admin','2021-04-12 15:05:11','黑名单类型'),(4,'BLACKLIST_TYPE','1','ip','admin','2021-04-12 15:06:41','admin','2021-04-12 15:06:45','黑名单类型');

/*Table structure for table `t_resource` */

DROP TABLE IF EXISTS `t_resource`;

CREATE TABLE `t_resource` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源描述',
  `seq` int NOT NULL COMMENT '资源序号',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源地址',
  `pid` int DEFAULT NULL COMMENT '父级ID',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '0-菜单，1-功能 引用数据字典表',
  `create_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建用户',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改用户',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_resource` */

insert  into `t_resource`(`id`,`name`,`remark`,`seq`,`url`,`pid`,`type`,`create_user`,`create_time`,`update_user`,`update_time`) values (1,'系统管理',NULL,100,'/system',NULL,'0','admin','2021-03-29 17:46:27','admin','2021-03-31 09:38:50'),(2,'资源管理',NULL,100,'/system/resource',1,'0','admin','2021-03-29 17:46:27','admin','2021-03-29 18:23:32'),(3,'角色管理',NULL,101,'/system/role',1,'0','admin','2021-03-29 17:46:27','admin','2021-03-29 18:21:23'),(4,'系统用户管理',NULL,102,'/system/sysuser',1,'0','admin','2021-03-29 17:46:27','admin','2021-03-29 17:46:27'),(5,'支付系统',NULL,102,'/pay',NULL,'0','admin','2021-03-29 17:46:27','admin','2021-04-12 09:34:00'),(6,'支付订单',NULL,100,'/order',5,'0','admin','2021-03-29 17:46:27','admin','2021-03-31 09:46:32'),(7,'销售系统',NULL,104,'/sales',NULL,'0','admin','2021-03-29 17:46:27','admin','2021-04-12 09:34:12'),(8,'用户',NULL,100,'/user',7,'0','admin','2021-03-29 17:46:27','admin','2021-03-31 10:10:42'),(9,'销售订单',NULL,100,'/order',7,'0','admin','2021-03-29 17:46:27','admin','2021-03-29 17:46:27'),(10,'商品','',100,'/commodity',7,'0','admin','2021-03-29 17:46:27','admin','2021-03-31 10:11:05'),(11,'添加资源',NULL,101,'/system/resource/add',2,'1','admin','2021-03-30 21:18:09','admin','2021-03-31 09:26:19'),(12,'修改资源','',102,'/system/resource/update',2,'1','admin','2021-03-30 21:27:06','admin','2021-03-31 09:26:25'),(13,'删除资源','',103,'/system/resource/delete',2,'1','admin','2021-03-30 22:05:46','admin','2021-03-31 09:26:29'),(14,'查询资源','',100,'/system/resource/list',2,'1','admin','2021-03-31 09:25:59','admin','2021-04-01 15:19:57'),(15,'添加角色','',101,'/system/role/add',3,'1','admin','2021-03-31 09:27:35','admin','2021-03-31 09:27:35'),(16,'查询角色','',100,'/system/role/list',3,'1','admin','2021-03-31 09:27:59','admin','2021-03-31 09:27:59'),(17,'修改角色','',102,'/system/role/update',3,'1','admin','2021-03-31 09:30:00','admin','2021-03-31 09:30:00'),(18,'删除角色','',103,'/system/role/delete',3,'1','admin','2021-03-31 09:30:35','admin','2021-03-31 09:30:35'),(19,'查询用户','',100,'/system/user/list',4,'1','admin','2021-03-31 09:32:31','admin','2021-03-31 09:32:31'),(20,'添加用户','',101,'/system/user/add',4,'1','admin','2021-03-31 09:32:55','admin','2021-03-31 09:32:55'),(21,'修改用户','',102,'/system/user/update',4,'1','admin','2021-03-31 09:33:21','admin','2021-03-31 09:33:21'),(22,'删除用户','',100,'/system/user/delete',4,'1','admin','2021-03-31 09:33:49','admin','2021-03-31 09:33:49'),(23,'用户加解锁','',104,'/system/user/operating',4,'1','admin','2021-03-31 09:35:50','admin','2021-03-31 09:35:50'),(24,'用户授权','',105,'/system/user/authorization',4,'1','admin','2021-03-31 09:36:48','admin','2021-03-31 09:36:48'),(25,'用户修改密码','',106,'/system/user/update/pwd',4,'1','admin','2021-03-31 09:38:00','admin','2021-03-31 09:38:00'),(26,'PayPal订单补单','',101,'/member/paypal/make',6,'1','admin','2021-03-31 09:52:52','admin','2021-04-01 15:12:56'),(27,'PayPal订单退款','',102,'/member/paypal/refund',6,'1','admin','2021-03-31 09:53:25','admin','2021-04-01 20:30:57'),(28,'DLocal订单补单','',103,'/member/dlocal/make',6,'1','admin','2021-03-31 10:01:45','admin','2021-04-01 20:31:02'),(29,'DLocal订单退款','',104,'/member/dlocal/refund',6,'1','admin','2021-03-31 10:03:34','admin','2021-04-01 15:13:35'),(30,'PayPal订单补单','',101,'/member/paypal/make',9,'1','admin','2021-03-31 09:52:52','admin','2021-04-01 15:13:57'),(31,'PayPal订单退款','',102,'/member/paypal/refund',9,'1','admin','2021-03-31 09:53:25','admin','2021-04-01 20:31:43'),(32,'DLocal订单补单','',103,'/member/dlocal/make',9,'1','admin','2021-03-31 10:01:45','admin','2021-04-01 20:31:51'),(33,'DLocal订单退款','',104,'/member/dlocal/refund',9,'1','admin','2021-03-31 10:03:34','admin','2021-04-01 15:14:15'),(34,'查询用户','',100,'/sales/user/list',8,'1','admin','2021-03-31 10:08:48','admin','2021-04-01 15:20:09'),(35,'查询商品','',100,'/sales/commodity/list',10,'1','admin','2021-03-31 10:10:07','admin','2021-04-01 15:16:55'),(36,'新增商品','',101,'/sales/commodity/add',10,'1','admin','2021-03-31 10:11:43','admin','2021-04-01 15:20:35'),(37,'修改商品','',102,'/sales/commodity/update',10,'1','admin','2021-03-31 10:12:09','admin','2021-04-01 15:20:44'),(38,'删除商品','',103,'/sales/commodity/delete',10,'1','admin','2021-03-31 10:12:29','admin','2021-04-01 15:20:51'),(39,'商品上下线','',104,'/sales/commodity/update/line',10,'1','admin','2021-03-31 10:13:41','admin','2021-04-01 15:20:57'),(40,'查询订单','',100,'/member/order/mark/list',6,'1','admin','2021-04-01 15:10:06','admin','2021-04-01 15:10:06'),(41,'查询订单','',100,'/member/order/mark/list',9,'1','admin','2021-04-01 15:10:53','admin','2021-04-01 15:19:03'),(43,'PayPal查询退款','',105,'/member/paypal/find',6,'1','admin','2021-04-01 17:58:03','admin','2021-04-01 20:31:19'),(44,'DLocal查询退款','',106,'/member/dlocal/find',6,'1','admin','2021-04-01 17:58:48','admin','2021-04-01 20:31:30'),(45,'PayPal查询退款','',105,'/member/paypal/find',9,'1','admin','2021-04-01 17:59:20','admin','2021-04-01 20:32:02'),(46,'DLocal查询退款','',106,'/member/dlocal/find',9,'1','admin','2021-04-01 17:59:47','admin','2021-04-01 20:32:07'),(47,'参数管理','',101,'/parameter',NULL,'0','admin','2021-04-12 09:33:47','admin','2021-04-12 09:33:47'),(48,'数据字典管理','',100,'/dictionaries',47,'0','admin','2021-04-12 09:36:15','admin','2021-04-12 10:13:02'),(49,'添加字典','',101,'/system/dictionary/add',48,'1','admin','2021-04-12 09:36:53','admin','2021-04-12 09:36:53'),(50,'查询字典','',100,'/system/dictionary/list',48,'1','admin','2021-04-12 09:39:17','admin','2021-04-12 09:39:17'),(51,'修改字典','',102,'/system/dictionary/update',48,'1','admin','2021-04-12 10:07:56','admin','2021-04-12 10:07:56'),(52,'删除字典','',103,'/system/dictionary/delete',48,'1','admin','2021-04-12 10:08:23','admin','2021-04-12 10:08:23'),(53,'名单管理','',100,'/black',NULL,'0','admin','2021-04-12 15:30:31','admin','2021-04-12 15:30:31'),(54,'黑名单管理','',100,'/list',53,'0','admin','2021-04-12 15:31:22','admin','2021-04-12 15:31:22'),(55,'查询黑名单','',100,'/system/black/list',54,'1','admin','2021-04-12 15:39:29','admin','2021-04-12 15:39:29'),(56,'添加黑名单','',101,'/system/black/add',54,'1','admin','2021-04-12 15:39:54','admin','2021-04-12 15:39:54'),(57,'修改黑名单','',102,'/system/black/update',54,'1','admin','2021-04-12 15:40:16','admin','2021-04-12 15:40:16'),(58,'删除黑名单','',103,'/system/black/delete',54,'1','admin','2021-04-12 15:40:36','admin','2021-04-12 15:40:36');

/*Table structure for table `t_role` */

DROP TABLE IF EXISTS `t_role`;

CREATE TABLE `t_role` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色描述',
  `update_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人员',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人员',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `pid` int DEFAULT NULL COMMENT '父级角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色表';

/*Data for the table `t_role` */

insert  into `t_role`(`id`,`name`,`remark`,`update_user`,`update_time`,`create_user`,`create_time`,`pid`) values (1,'超管','','admin','2021-03-02 10:13:30','admin','2021-03-02 10:13:30',NULL),(3,'销售','销售','admin','2021-04-01 18:08:26','admin','2021-03-31 09:28:28',1);

/*Table structure for table `t_role_resource` */

DROP TABLE IF EXISTS `t_role_resource`;

CREATE TABLE `t_role_resource` (
  `role_id` int NOT NULL COMMENT '角色id',
  `resource_id` int NOT NULL COMMENT '资源id',
  PRIMARY KEY (`role_id`,`resource_id`) USING BTREE,
  KEY `TROLE_ID` (`role_id`) USING BTREE,
  KEY `idx_roleresource_roldid` (`role_id`) USING BTREE,
  KEY `idx_roleresource_resourceid` (`resource_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='角色资源表';

/*Data for the table `t_role_resource` */

insert  into `t_role_resource`(`role_id`,`resource_id`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,40),(1,41),(1,43),(1,44),(1,45),(1,46),(1,47),(1,48),(1,49),(1,50),(1,51),(1,52),(1,53),(1,54),(1,55),(1,56),(1,57),(1,58),(3,1),(3,2),(3,3),(3,4),(3,7),(3,8),(3,9),(3,10),(3,14),(3,15),(3,16),(3,17),(3,18),(3,19),(3,20),(3,21),(3,22),(3,23),(3,24),(3,25),(3,30),(3,31),(3,32),(3,33),(3,34),(3,35),(3,36),(3,37),(3,38),(3,39),(3,41),(3,45),(3,46);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电子邮箱',
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '状态1：正常，0：锁定',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人员',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人员',
  `pid` int DEFAULT NULL COMMENT '父级用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`username`,`name`,`phone`,`email`,`password`,`status`,`last_login_date`,`create_time`,`create_user`,`update_time`,`update_user`,`pid`) values (1,'admin','系统管理员','',NULL,'$2a$10$ECSsGGPxjsEWk27ue.TIxum63qXrNIKUsrAtMAM0QIqRmjTMgNuLe','1','2019-11-07 15:34:57','2016-05-04 13:48:25','admin','2019-09-01 10:39:22','admin',NULL),(2,'test','test','','','$2a$10$vkI3V.bglv8AoThWua.lQegyLIaRRmaSHpCQNDN8qEKKs020yUXb2','1',NULL,'2021-03-31 09:31:25','admin','2021-03-31 09:31:25','admin',1);

/*Table structure for table `t_user_role` */

DROP TABLE IF EXISTS `t_user_role`;

CREATE TABLE `t_user_role` (
  `user_id` int NOT NULL COMMENT '用户id',
  `role_id` int NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE,
  KEY `idx_roleuser_roldid` (`role_id`) USING BTREE,
  KEY `idx_roleuser_userid` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `t_user_role` */

insert  into `t_user_role`(`user_id`,`role_id`) values (1,1),(2,3);

/*Table structure for table `undo_log` */

DROP TABLE IF EXISTS `undo_log`;

CREATE TABLE `undo_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `branch_id` bigint NOT NULL,
  `xid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rollback_info` longblob NOT NULL,
  `log_status` int NOT NULL,
  `log_created` datetime NOT NULL,
  `log_modified` datetime NOT NULL,
  `ext` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `undo_log` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
